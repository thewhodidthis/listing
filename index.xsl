<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="utf-8" indent="no" />
  <xsl:variable name="apos" select='"&#39;"' />
  <xsl:template name="date">
    <xsl:param name="stamp" />
    <xsl:param name="Y" select="substring(substring($stamp, 1, 4), 3, 4)" />
    <xsl:param name="m" select="substring($stamp, 6, 2)" />
    <xsl:param name="M" select="substring('JanFebMarAprMayJunJulAugSepOctNovDec', 3 * ($m - 1) + 1, 3)" />
    <xsl:param name="d" select="substring($stamp, 9, 2)" />
    <xsl:param name="t" select="substring($stamp, 12, 5)" />
    <xsl:value-of select="concat($d, '&#160;', $M, '&#160;', $apos, $Y, '&#160;', $t)" />
  </xsl:template>
  <xsl:template name="size">
    <xsl:param name="bytes" />
    <xsl:choose>
      <xsl:when test="$bytes &lt; 1000">
        <xsl:value-of select="format-number($bytes, '0')" />
        <xsl:text>&#160;bytes</xsl:text>
      </xsl:when>
      <xsl:when test="$bytes &lt; 1048576">
        <xsl:value-of select="format-number($bytes div 1024, '0.00')" />
        <xsl:text>&#160;Kb</xsl:text>
      </xsl:when>
      <xsl:when test="$bytes &lt; 1073741824">
        <xsl:value-of select="format-number($bytes div 1048576, '0.00')" />
        <xsl:text>&#160;Mb</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number($bytes div 1073741824, '0.00')" />
        <xsl:text>&#160;Gb</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="directory">
    <dl>
      <dt><a href="{current()}"><strong><xsl:value-of select="." /></strong></a></dt>
      <dd>
        <xsl:call-template name="date">
          <xsl:with-param name="stamp" select="@mtime" />
        </xsl:call-template>
      </dd>
      <dd>-</dd>
    </dl>
  </xsl:template>
  <xsl:template match="file">
    <dl>
      <dt><a href="{current()}"><xsl:value-of select="." /></a></dt>
      <dd>
        <xsl:call-template name="date">
          <xsl:with-param name="stamp" select="@mtime" />
        </xsl:call-template>
      </dd>
      <dd>
        <xsl:call-template name="size">
          <xsl:with-param name="bytes" select="@size" />
        </xsl:call-template>
      </dd>
    </dl>
  </xsl:template>
  <xsl:param name="title" select="'/'" />
  <xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html lang="en">
      <head>
        <title><xsl:value-of select="$title" /></title>
        <style>
          html {
            font: 16px/1 sans-serif;
            height: 100%;
          }
          body {
            align-items: center;
            display: flex;
            margin: 0;
            min-height: 100%;
          }
          a {
            text-decoration: none;
          }
          a:not([href*="."]) {
            font-size: 0.875rem;
            font-weight: bold;
          }
          a:hover {
            color: #f00;
          }
          p {
            margin: 0;
          }
          main {
            box-sizing: border-box;
            flex: 1;
            font-size: 0.75rem;
            margin: 0;
            padding: 2rem;
          }
          dl {
            border-top: 1px dotted #ddd;
            box-sizing: border-box;
            margin: 0 auto;
            padding: 0 0.5rem;
          }
          dl:last-of-type {
            border-bottom: 1px dotted #ddd;
          }
          dl:hover {
            background: #f9f9f9;
          }
          dt {
            font-size: 0.9375rem;
            width: 50%;
          }
          dd {
            font-family: 'Menlo', 'Andale Mono', monospace;
            margin: 0;
            text-align: right;
            width: 25%;
          }
          dt,
          dd {
            box-sizing: border-box;
            display: inline-block;
            overflow: hidden;
            padding: 0.5rem 0.25rem;
            text-overflow: ellipsis;
            vertical-align: middle;
            white-space: nowrap;
          }
          footer {
            color: #999;
            margin-top: 2rem;
            text-align: center;
          }
          footer a {
            color: inherit;
          }
          footer span::before {
            content: " ";
          }
          footer span::after {
            content: ", ";
          }
          footer span:last-of-type::after {
            content: "";
          }
          footer p:last-child {
            font-size: 0.6875rem;
            margin-top: 0.5rem;
          }
        </style>
      </head>
      <body>
        <main>
          <xsl:apply-templates />
          <footer>
            <p>
              <xsl:variable name="d-count" select="count(//directory)" />
              <xsl:if test="boolean($d-count)">
                <strong>
                  <xsl:number value="$d-count" format="I" />
                </strong>
                <span>
                  <xsl:text>directories</xsl:text>
                </span>
              </xsl:if>
              <xsl:variable name="f-count" select="count(//file)" />
              <xsl:if test="boolean($f-count)">
                <strong>
                  <xsl:number value="$f-count" format="I" />
                </strong>
                <span>
                  <xsl:text>files</xsl:text>
                </span>
                <strong>
                  <xsl:call-template name="size">
                    <xsl:with-param name="bytes" select="sum(//file/@size)" />
                  </xsl:call-template>
                </strong>
                <span>
                  <xsl:text>total</xsl:text>
                </span>
              </xsl:if>
            </p>
            <p><a href="https://thewhodidthis.com">thewhodidthis.com</a></p>
          </footer>
        </main>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
