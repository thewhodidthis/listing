## about

Templates for dressing up directory listings in PHP and XSLT and Caddy.

## setup

Fetch latest from GitLab:

```sh
git clone https://gitlab.com/thewhodidthis/listing.git path/to/listing
```

## usage

Link up inside of your server configuration file. If using `nginx(8)` for example,

```
server {
  location / {
    autoindex on;
    autoindex_format xml;

    xslt_string_param title "TiTle";
    xslt_stylesheet path/to/listing/index.xsl;
  }
}
```
