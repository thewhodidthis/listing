<?php

/**
 * A very simple index page.
 *
 * Place in a PHP serving location to list its contents.
 */

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $_SERVER['REQUEST_URI']; ?></title>
        <style>
            html {
                font: 16px/1 sans-serif;
                height: 100%;
            }
            body {
                align-items: center;
                display: flex;
                margin: 0;
                min-height: 100%;
            }
            a {
                text-decoration: none;
            }
            a:not([href*="."]) {
                font-size: 0.875rem;
                font-weight: bold;
            }
            a:hover {
                color: #f00;
            }
            ul {
                box-sizing: border-box;
                flex: 1;
                margin: 0;
                padding: 2rem;
                width: 100%;
            }
            li {
                border-top: 1px dotted #ddd;
                display: flex;
            }
            li:last-child {
                border-bottom: 1px dotted #ddd;
            }
            li:hover {
                background: #f9f9f9;
            }
            dl {
                font-size: 0.75rem;
                margin: 0 auto;
                max-width: 50rem;
                width: 100%;
            }
            dt {
                font-size: 0.9375rem;
                width: 50%;
            }
            dd {
                font-family: 'Menlo', 'Andale Mono', monospace;
                margin: 0;
                text-align: right;
                width: 25%;
            }
            dt,
            dd {
                box-sizing: border-box;
                display: inline-block;
                overflow: hidden;
                padding: 0.5rem 0.25rem;
                text-overflow: ellipsis;
                vertical-align: middle;
                white-space: nowrap;
            }
        </style>
    </head>
    <body>
        <ul>
            <?php
            // Exclude dotfiles and non-readable items
            $contents = array_filter(preg_grep('/^([^.])/', scandir(getcwd())), 'is_readable');

            foreach ($contents as $item) {
                $basename = basename($item);
                $date = date("d M 'y H:i:s", filemtime($item));
                $size = '-';

                if (is_file($item)) {
                    // https://gist.github.com/liunian/9338301
                    $filesize = filesize($item);
                    // This would be 0 for bytes, 1 for Kb, etc.
                    $exponent = floor(log($filesize, 1024));
                    $unit = ['bytes', 'Kb', 'Mb', 'Gb'][$exponent];
                    $size = round($filesize / pow(1024, $exponent), [0, 0, 2, 2][$exponent]) . " {$unit}";
                }

                printf(
                    '<li><dl><dt><a href="%1$s" title="%2$s">%1$s</a></dt><dd>%3$s</dd><dd>%4$s</dd></dl></li>',
                    $item,
                    $basename,
                    $date,
                    $size
                );
            } ?>

        </ul>
    </body>
</html>
